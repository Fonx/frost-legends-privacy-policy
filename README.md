# Frost Legends Privacy Policy

This Privacy Policy describes how Frost Legends ("we," "our," or "us") collects, uses, and shares your information when you use our mobile application (the "App").

Collection and Use of Information

When you play our game, we may collect the following information:

Usage Information: We may collect information about how you use the App, such as the actions you take during the game, information about your mobile device, your IP address, and the type of browser you are using.

Payment Information: If you choose to make a purchase within the App, Unity inapp-purchase may collect payment information.

Information Provided by You: We may collect information that you provide to us, such as your name and email address. This information may be used to provide user support and to contact you regarding important game updates or for promotional purposes.

Sharing of Information

We do not share personal information with third parties, except when necessary to provide user support, for payment processing purposes, or as required by law.

Security of Information

We take reasonable measures to protect the information collected through the App from loss, misuse, and unauthorized access, disclosure, alteration, and destruction.

Retention of Information

We retain your information for as long as necessary to provide the requested service and as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.

Changes to the Privacy Policy

We may update this Privacy Policy periodically. When we make material changes, we will update the "Last Updated" date at the top of this Privacy Policy.

Contact Us

If you have questions about this Privacy Policy or wish to request the deletion of your personal information, please contact us at tropicalthreestudios@gmail.com.

Last Updated: 02/22/2023
